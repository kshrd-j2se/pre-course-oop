package org.kshrd.superkeyword;

class X {

    public X() {
        System.out.println("X con");
    }
    public X(String a) {
        System.out.println("X constructor");
    }
}

class Y extends X {

    public Y() {
        this("Hello");
        //super(); // it is also put event we don't call it
        System.out.println("Y constructor");
    }


    public Y(String st) {
        System.out.println("Hello");
    }


    public static void main(String[] args) {
        new Y("hello");
    }


    // method overloading: number of methods more than 1,
    // but different method signature


    void a(int a) {

    }

    // a("adfsf");

    void a() {

    }

    void a(String a) {

    }
}
