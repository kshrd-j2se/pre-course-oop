package org.kshrd.superkeyword;

class A0 {
    public int id = 30;
    public void a() {
        System.out.println("a0");
    }
}

class A extends A0 {
    public int id = 20;
    @Override
    public void a() {
        System.out.println("a");
    }
}

class B extends A {
    int id = 10;

    public void a() {
        System.out.println("A in b");
    }

    void b() {
        System.out.println("Id : " + super.id);
        a();
    }

    public static void main(String[] args) {
         new B().b();
        // 10 , 20
    }

}


// Perents - child,
// base - derive
// super - sub
// => is-a relationship (inheritance)