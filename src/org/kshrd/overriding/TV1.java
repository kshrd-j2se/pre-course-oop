package org.kshrd.overriding;

public class TV1 {

    protected void changeChannel() {
        System.out.println("Change by manual");
    }

}

class TV2 extends TV1 {

    public void changeChannel(String method) {
        System.out.println("Change by voice");
    }

    @Override
    public final void changeChannel() {
        System.out.println("Change by remote");
    }

    void call() {
        super.changeChannel();
    }

    public static void main(String[] args) {
//        new TV2().call();

//         TV2 tv2 = new TV2();
//         TV1 tv1 = new TV1();
          TV1 tv1 = new TV2();
          tv1.changeChannel();

    }

}
