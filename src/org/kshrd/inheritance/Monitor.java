package org.kshrd.inheritance;

import java.security.PublicKey;

public interface Monitor {
    void turnOff();
    void turnOn();
    void volumUp();
    void volumDown();
}

// IS-A
class DellMonitor implements Monitor {

    public boolean hasSpeaker = true;




    @Override
    public void turnOff() {
        System.out.println("use button");
    }

    @Override
    public void turnOn() {

    }

    @Override
    public void volumUp() {

    }

    @Override
    public void volumDown() {

    }
}

class SonyMonitor implements Monitor {
    @Override
    public void turnOff() {
        System.out.println("use remote controle");
    }

    @Override
    public void turnOn() {

    }

    @Override
    public void volumUp() {

    }

    @Override
    public void volumDown() {

    }
}




// ClassA extends ClassB
// ClassA implements Interface
// InterfaceA extends InterfaceB

// Interface extends Class cannot
