package org.kshrd.inheritance;

// Lexus is-a Car
public class Lexus extends Car {

    private int numberOfRadio = 1;

    public int getNumberOfRadio() {
        return numberOfRadio;
    }

    // overriding
    public void gearBreak() {
        System.out.println("I use digital for break stop my card");
    }


}
