package org.kshrd.inheritance;

public class Car {

    private int numberOfWheel = 4;
    public int getNumberOfWheel() {
        return numberOfWheel;
    }
    public void setNumberOfWheel(int numberOfWheel) {
        this.numberOfWheel = numberOfWheel;
    }
    public void gearUp() {
        System.out.println("Gear up");
    }
    public void gearDown() {
        System.out.println("Gear down");
    }
    public void gearBreak() {
        System.out.println("Gear break");
    }

}
