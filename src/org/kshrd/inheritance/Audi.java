package org.kshrd.inheritance;


// is-a relationship
// Audi is-a Car
public class Audi extends Car {



    // HAS-A relationship
    // Audi has-an Engine
    Engine engine = new Engine();

    public void maintenance() {
        engine.coolDown();
    }

}
