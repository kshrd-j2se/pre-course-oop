package org.kshrd.inheritance;

public class Main {

    public static void main(String[] args) {
        // has-a relationship
        Audi audi = new Audi();
        audi.gearBreak();

    }

}
