package org.kshrd;

public class Sharp {

    // Overloading method of draw()
    public void draw() {
        System.out.println("Draw in Sharp");
    }

    public void draw(String message) {
        System.out.println("Draw in Shap " + message);
    }

}
