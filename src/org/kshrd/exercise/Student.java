package org.kshrd.exercise;

public class Student extends Person {

    private double math=10, english, chemistry, biology, khmer, average;

    public double getMath() {
        return math;
    }

    public void setMath(double math) {
        if (math<0 || math>100)
            return;
        this.math = math;
    }

    public double getEnglish() {
        return english;
    }

    public void setEnglish(double english) {
        this.english = english;
    }

    public double getChemistry() {
        return chemistry;
    }

    public void setChemistry(double chemistry) {
        this.chemistry = chemistry;
    }

    public double getBiology() {
        return biology;
    }

    public void setBiology(double biology) {
        this.biology = biology;
    }

    public double getKhmer() {
        return khmer;
    }

    public void setKhmer(double khmer) {
        this.khmer = khmer;
    }

    public double findAverage(double math, double english, double chemistry, double biology, double khmer ) {

        double avg = (math + english + chemistry + biology + khmer) / 5;
        return avg;
    }

    public double findAverage() {
        average = (math + english + chemistry + biology + khmer) / 5;
        return average;
    }

    @Override
    public String toString() {
        return "Student{" +
                "math=" + math +
                ", english=" + english +
                ", chemistry=" + chemistry +
                ", biology=" + biology +
                ", khmer=" + khmer +
                ", Average=" + average+
                '}';
    }
}

