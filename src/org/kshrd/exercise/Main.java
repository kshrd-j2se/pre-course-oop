package org.kshrd.exercise;

public class Main {

    public static void main(String[] args) {
        Student student = new Student();
        student.setId(1);
        student.setName("Bopha");
        student.setGender("Female");
        student.setDateOfBirth("1999-01-23");
        student.setMath(-80);
        student.setChemistry(90);
        student.setBiology(100);
        student.setKhmer(95);
        student.setEnglish(70);
        double avg = student.findAverage();
        System.out.println(student);

    }
}
