package org.kshrd.exercise;

public class Employer extends Person {

    private double salary;

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double findSalary() {
        // you may need to calculate something before
        // return value to the mothod
        return 300;
    }



}
